#!/usr/bin/env bash

cat <<EOF
[input]
base_directory = "content/artigos/"
stemming = "Portuguese"
url_prefix = "https://drmpt.pxto.pt/"
frontmatter_handling = "Parse"
files = [
EOF

find content/artigos -name "*.md" |
  sed -e 's|content/artigos/||' |
  while read filename; do
    slug="$(echo "$filename" | sed -e 's|/index.md||')"
    date="$(cat "content/artigos/$filename" | yq --front-matter=extract .date | sed -e 's/ ..:..:..//' -e 's/-/\//g')"
    echo "  {url=\"$date/$slug\", title=\"\", path=\"$filename\"},"
  done

echo "]"
