#!/usr/bin/env bash

mkdir -p content/archd
mkdir -p content/archm
mkdir -p content/archy

grep -Rhm 1 ^date: content/artigos | sed -e 's/^date: \(....\)-..-.. ..:..:../\1/' | sort -u |
  while read archy; do echo -e "---\ndate: $archy-01-01 00:00:00\n---" > content/archy/$archy.md; done
grep -Rhm 1 ^date: content/artigos | sed -e 's/^date: \(....-..\)-.. ..:..:../\1/' | sort -u |
  while read archm; do echo -e "---\ndate: $archm-01 00:00:00\n---" > content/archm/$archm.md; done
grep -Rhm 1 ^date: content/artigos | sed -e 's/^date: \(....-..-..\) ..:..:../\1/' | sort -u |
  while read archd; do echo -e "---\ndate: $archd 00:00:00\n---" > content/archd/$archd.md; done
