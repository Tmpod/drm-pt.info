---
title: O que é o DRM?
---

DRM é uma sigla, que significa, _Digital Rights Management_, embora seja mais
apropriado dizer _Digital **Restrictions** Management_ ou, em Português, Gestão
Digital de Restrições. Estas tecnologias são commumente chamadas de tecnologias
anti-cópia ou contra a cópia e estão referidas na lei portuguesa (Código de
Direito de Autor e Direitos Conexos) como Medidas Tecnológicas ou de Carácter
Tecnológico.

Como o seu nome indica, a gestão de direitos digitais aplica-se somente aos
meios digitais. O conteúdo digital tem ganho popularidade sobre o conteúdo
analógico por dois motivos: o primeiro deve-se ao facto das vantagens técnicas
associadas com a sua produção, reprodução e manipulação e o segundo porque, na
maioria das vezes, a qualidade é superior em relação ao analógico. Desde o
nascimento dos computadores pessoais, os arquivos de conteúdo digital
tornaram-se um meio fácil de fazer cópias de modo ilimitado sem aparecer
qualquer perda na qualidade das cópias subsequentes. Muito conteúdo analógico
perde qualidade com cada geração copiada e frequentemente durante o seu uso
normal. A popularidade da Internet e das ferramentas para partilhar arquivos
simplificou a distribuição de conteúdo digital.

A disponibilidade de múltiplas cópias perfeitas de material sujeito a direitos
de autor foi entendida pela indústria como um golpe ao seu modelo de negócio,
em especial dentro da indústria fonográfica, cinematográfica e dos jogos
electrónicos. Estes modelos de negócio tradicionais recaem na habilidade de
obter lucro por cada cópia feita do trabalho digital, e algumas vezes por cada
execução daquele material. O DRM foi criado e planeado por essas empresas e
indivíduos, que tentam transpor esses modelos de negócio tradicionais para o
mundo digital, mas com medidas para restringir a duplicação e disseminação do
seu conteúdo.
