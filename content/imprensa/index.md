---
title: Imprensa
---

## Flyers

- DRM em eBooks [[PDF][1] | [ODT][2]]

## Apresentações

- [Apresentação sobre o DRM na Indústria Discográfica [PDF]][3]
- [Transcrição de uma apresentação sobre o DRM (em Inglês)][4]

## Artigos

- [Artigo em Português intitulado “DRM e as suas consequências”][5]
- [Artigo de Opinião no Diário Económico intitulado “O dinheiro dos músicos”][6]
- [“Anti-DRM”, artigo que explica o que é que são os DRM e quais os seus problemas][7]
- [O fim do DRM e uma nova era na música][8]

## Filmes

- [Animação em Flash, em Português][9]
- [Filme da acção de sensibilização de 25 de Maio][10]
- [Palestra sobre DRM][11]

## Outros sites

- [Campanha Restrições Tecnológicas: você paga e leva menos – site Brasileiro sobre DRM][12]
- [Defective By Design – site da Free Software Foundation][13]

[1]: 
[2]:
[3]: https://github.com/marado/DRM-presentation/blob/master/music-industry.pdf
[4]: https://web.archive.org/web/20150316045812/http://www.dashes.com/anil/stuff/doctorow-drm-ms.html
[5]: https://web.archive.org/web/20070711080900/http://deinix.dei.uc.pt/zine/3/02-DRM.pdf
[6]: https://web.archive.org/web/20080225114247/http://diarioeconomico.sapo.pt/edicion/diarioeconomico/opinion/columnistas/pt/desarrollo/736334.html
[7]: http://oterceiroolho.lusopt.info/anti-digital-rights-management/23/09
[8]: https://web.archive.org/web/20070510222650/http://expresso.clix.pt/COMUNIDADE/blogs/paulo_querido/archive/2007/04/06/31886.aspx
[9]: https://web.archive.org/web/20070710100617/http://drm.ideias3.com/drm.swf
[10]: https://www.youtube.com/watch?v=-VKZcfohlt8
[11]: https://www.youtube.com/watch?v=7v7UiqmeSTw
[12]: https://web.archive.org/web/20080720004814/http://www.idec.org.br/restricoestecnologicas/
[13]: https://www.defectivebydesign.org/
