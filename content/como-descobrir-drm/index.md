---
title: Como descobrir DRM
---

Nesta página encontra uma lista, que vai sendo actualizada, com produtos que
têm DRM e empresas que apoiam ou incentivam a utilização de DRM. Se encontrou
um produto com DRM que não esteja aqui listado, coloque nos comentários.
Ajude-nos a completar esta lista!

## Música

- CD corrompidos, das editoras\*:
  - Sony
  - Warner
  - Universal
  - Blue Note Records (pertence à Universal)
  - …
- iOS (iPod, iPad e iPhone)
- Zen da Creative
- Zune da Microsoft


## Vídeo

DVDs de:

- BBC
- Castello Lopes
- Fortíssimo Films
- LNK Audiovisuais
- Planeta DeAgostini
- Prisvideo
- Sony
- NOS (Lusomundo)


## Jogos Electrónicos

- Electronic Arts
- PlayStation 2 e PlayStation Portable
- XBox e XBox 360

## Livros Digitais

- LeYa
- Wook/Porto Editora
- Bertrand
- Fnac/Kobo\*\*
- iTunes Store
- Amanzon/Kindle\*\*


## Áudio Livros

- iTunes Store
- Audible


## Informática

- Windows Vista (e Windows 7)
- Computadores Vista Ready (se quiser saber como comprar um computador sem Sistema Operativo em Portugal veja aqui)
- Campanha Trusted Computing Platform Alliance
- MacBook
- Todos os computadores com selo de Windows 8 têm DRM para o impedir de instalar outro Sistema Operativo

## Empresas que usam ou promovem DRM

- Adobe
- Alcatel-Lucent
- Apple
- AMD
- Amazon
- Ascent Media
- BBC
- Bertrand
- Best Buy
- Bioware
- Blueprint
- BT
- CableLabs
- Catch Media
- CinemaNow
- Cineplex Entertainment
- Cisco
- Comcast
- Cox Communications
- Creative
- CSG Systems
- Deluxe
- Disney
- DivX
- Dolby
- DTS
- EMI
- EuEbooks
- ExtendMedia
- Flor De Utopia
- Fox Entertainment
- Google
- HarperCollins
- HP (Hewlett-Packard)
- Huawei Technologies
- IBM
- Intel
- Irdeto
- LeYa
- LG
- Liberty Global
- Lionsgate
- LOVEFiLM
- Lusomundo
- Marka
- Marvell Semiconductor
- Matsushita (Panasonic)
- MGM
- Microsoft
- MOD Systems
- Motorola
- MPO
- MyEBooks
- Nagravision
- NBC Universal
- NDS Group
- Netflix
- Neustar
- Nokia
- Nvidia
- Panasonic
- Paramount Pictures
- Penguin
- Philips
- PortoEditora
- Red Bee Media
- Rhapsody
- RIAA
- Rovi
- Saffron Digital
- Samsung
- Secure Path
- Sonic Solutions
- Sony
- Spotify
- Switch Communications
- Tesco
- Thomson
- Toshiba
- Twentieth Century Fox
- Universal
- Verimatrix
- VeriSign
- Warner Brothers
- Zoran

\* – Nos últimos tempos, e pressionadas pelos consumidores, estas editoras
começaram a deixar de colocar DRM em alguns produtos, mas os CD e MP3
publicados anteriormente continuam a ter DRM.

\*\* – Nestes casos, os detentores de direitos têm a hipótese de escolher que
as suas obras não tenham DRM, mas os consumidores não têm nenhuma forma de
saber, antes de comprar, se o livro tem ou não DRM.
