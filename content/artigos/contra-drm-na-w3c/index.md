---
title: ANSOL e AEL organizam demonstração contra algemas digitais no encontro da W3C
author: Marcos Marado
date: 2016-09-08 23:29:00
categories:
- Legislação
- Protesto
tags:
- HTML
- Protesto
- W3C
---

A [ANSOL – Associação Nacional para o Software Livre][1] e a [AEL – Associação
Ensino Livre][2] vão manifestar-se contra a incorporação de Medidas de Carácter
Tecnológico (DRM) no HTML, a norma técnica que define a Web. A ANSOL e a AEL
convidam todos os interessados a juntarem-se a eles no dia 21 de Setembro às
18:00 no Centro de Congressos de Lisboa, durante um encontro da World Wide Web
Consortium (W3C), que define normas para a Web como o HTML e o CSS. Membros da
W3C como a Microsoft, a Google e a Netflix têm vindo a fazer pressão para a
incorporação de [Encrypted Media Extensions (EME)][3] no HTML. Isto faria com
que o HTML deixasse de ser uma norma aberta, de acordo com a legislação
nacional. EME no HTML faria com que ele falhasse três dos cinco critérios na
[Lei das Normas Abertas (39/2011)][4] e iria sacrificar injustificadamente a
liberdade na Web.

“Um dos grandes problemas em ter DRM inserido na especificação do HTML é que o
DRM em si é composto por acções e processos não documentados, o que significa
que o HTML5 passaria a não ser uma norma aberta de acordo com a Lei das Normas
Abertas”, diz Marcos Marado, presidente da ANSOL, acrescentando, “isto por si
só tornaria o HTML5 inviável para ser usado pela Administração Pública
Portuguesa, e noutros países que, como em Portugal, mandatam – e bem – que
apenas Normas Abertas podem ser usadas.”

Empresas de streaming como o Netflix requerem que os utilizadores usem DRM —
também conhecido como “algemas digitais” — nos seus próprios dispositivos, para
os impedir de fazer operações que essas empresas não permitem nos media
digitais, ainda que sejam permitidas por lei. “O DRM é conhecido por espiar os
utilizadores, colocá-los em perigo ao expô-los a vulnerabilidades de segurança,
e limitando-osao tirar-lhes controlo dos seus próprios computadores”, diz Paula
Simões, presidente da AEL. O DRM já existe na Web, mas não na sua
especificação. Tecnologistas e activistas das liberdades digitais avisam que
DRM nas normas da Web tornarão mais barato e menos custoso politicamente impor
restrições aos utilizadores, precipitando um aumento do DRM na Web.

O protesto, com o apoio da [Free Software Foundation][5], irá ocorrer a 21 de
Setembro, das 18:00 às 22:00, no Centro de Congressos de Lisboa, onde a W3C irá
ter o seu encontro. Os organizadores dão as boas vindas a todos aqueles que se
preocupam com a liberdade na Internet e no software que queiram comparecer.

A W3C é um organismo não-governamental de tomada de decisões, constituída por
corporações, entidades sem fins lucrativos e Universidades. A adição de DRM à
norma HTML chama-se Encrypted Media Extentions e está a ser desenvolvida por um
grupo de tecnologistas da indústria a trabalhar sob a alçada da W3C. É
actualmente um rascunho em processo de revisão e testes, mas é esperado que até
ao final de 2016 a W3C vote a sua ratificação como norma oficial para a Web.

Este ano, a W3C tem sido seguida por protestos contra o DRM. A última grande
reunião da W3C, que ocorreu em Março de 2016, foi [acolhida com a primeira
demonstração de sempre][6] numa reunião deste organismo.

Zak Rogoff, gestor de campanhas da Free Software Foundation, organizou o
protesto de Março. Disse: “Utilizadores da Web em todo o mundo estão
preocupados com este esquema da indústria para criar um sistema de DRM
universal para a Web. Inspiramo-nos com a ANSOL, AEL, e os activistas que irão
protestar no encontro da W3C em Lisboa. Fazemos todos parte de um movimento
unido pela liberdade na Internet, e a W3C não pode ignorar as nossas
preocupações.”

---

A ANSOL – Associação Nacional para o Software Livre é uma associação portuguesa
sem fins lucrativos que tem como fim a divulgação, promoção, desenvolvimento,
investigação e estudo da Informática Livre e das suas repercussões sociais,
políticas, filosóficas, culturais, técnicas e científicas.

A Associação Ensino Livre (AEL) tem como objectivos a promoção e utilização de
Software Livre e de Conteúdos Livres, nomeadamente com licenças Creative
Commons e Open Access ao nível do ensino, em Portugal, trabalhando para isso
com professores, alunos, investigadores, bibliotecas e instituições educativas.

[1]: https://ansol.org
[2]: https://ensinolivre.pt/
[3]: https://www.w3.org/TR/encrypted-media/
[4]: https://m6.ama.pt/docs/Lei362011-NormasAbertas.pdf
[5]: https://www.fsf.org/
[6]: https://www.defectivebydesign.org/from-the-web-to-the-streets-protesting-drm
