---
title: HTML, EME e Normas Abertas
author: Marcos Marado
date: 2016-09-18 20:26:00
carrousel: true
categories:
- Factos
- HTML
- Legislação
- Normas Abertas
- Protesto
- W3C
---

Normas Abertas são normas (especificações de formatos ou protocolos) que
cumprem [cinco simples regras][1]. Elas têm de ser:

1. sujeitas a escrutínio público completo e sem restrições de uma forma
   igualmente disponível a todas as partes;
2. sem nenhum componente ou extensão que tenha dependências em formatos ou
   protocolos que não cumpram, eles próprios, a definição de uma Norma Aberta;
3. livres de cláusulas legais ou técnicas que limitam a sua utilização por
   qualquer parte ou em qualquer modelo de negócio;
4. geridas e desenvolvidas continuamente de forma independente de um único
   fornecedor num processo aberto à igual participação de competidores e outras
   partes;
5. disponível em múltiplas implementações completas por fornecedores
   competidores, ou como uma implementação completa disponível de igual forma a
   todas as partes.

A importância das Normas Abertas é de certa forma sublinhada pela sua própria
definição: uma Norma Aberta pode facilmente ser usada por qualquer pessoa, sem
restrições. Isto permite a qualquer programador implementar essa norma, e evita
exclusão de utilizadores – qualquer pessoa é livre de usar uma Norma Aberta.
Esta importância foi reconhecida ao longo dos anos, e ultimamente esse
reconhecimento chegou a altos níveis de definição de políticas, com países como
Portugal a [mandatar o uso exclusivo de Normas Abertas pela Administração
Pública][2], como forma de garantir aos seus cidadãos acesso sem restrições a
dados e serviços públicos.

Um bom exemplo de uma Norma Aberta é o HTML. Mesmo que não esteja muito
à-vontade com questões técnicas, provavelmente já ouviu falar do HTML — ou pelo
menos notado em alguns “endereços web” (chamados URL) como
“http://umsite.pt/isto.html”. Isso acontece porque o HTML é a “linguagem da
web”: cada página web é escrita num formato, chamado HTML, e visto que todas as
páginas web são escritas no mesmo formato, e porque esse formato é uma Norma
Aberta, qualquer pessoa pode criar um programa de computador (neste caso, um
navegador web) que recebe fielmente a página nesse formato e sabe como
mostrá-la correctamente no ecrã do seu computador. Isto é o que torna possível
ver a mesma página web no seu telemóvel, o seu computador ou no seu tablet,
mesmo que esteja a usar programas diferentes para a ver. Não interessa se está
a usar o navegador web do Android no seu telemóvel ou o Firefox no seu
computador, enquanto um amigo seu está a usar o navegador do iOS e o Safari no
seu Mac. Qualquer pessoa pode ver uma página web correctamente: isso é uma das
belezas da Web, e acontece porque o HTML é uma Norma Aberta.

Então, o que é isso do EME? E porque é que as pessas andam a falar sobre “DRM
no HTML”?

O EME é uma especificação proposta que tem como intenção “[extender o
HTMLMediaElement][3]” (algo que é parte da especificação do HTML). O que isso
significa é que, se o EME for aprovado, uma parte da especificação actual do
HTML será actualizada. A “nova versão” incuirá dois tipos de alterações:
algumas mandatórias e outras opcionais. Nas suas próprias palavras,
“Implementação de Digital Rights Management não é obrigatória para cumprir com
esta especificação: apenas o sistema Clear Key é de implementação obrigatória
como base comum.” A [W3C argumenta][4] que isto não constitui nenhum problema,
e que não estão a acrescentar DRM no HTML, porque implementar DRM não é
obrigatório. Contudo, o facto de implementar DRM ser possível (mesmo que não
obrigatório) significa que, se um sítio web usar tal implementação, ou o seu
navegador web também a usa, ou não será capaz de ver essa parte do site
(lembra-se do que disse atrás sobre todos poderem ver a web? Isso deixaria de
acontecer.) Claro que isto não necessitaria de ser um problema de todo: se a
especificação permite a implementação, é só uma questão de todos os navegadores
fazerem essa implementação, certo? Errado. O problema aqui é que a
“especificação não define um sistema de protecção de conteúdos ou Digital
Rights Management”.

Por outras palavras: o EME introduz a possibilidade de usar coisas (sistemas de
DRM) que não fazem parte da especificação, pelo que uma especificação (uma
forma de uma parte independente implementar toda a especificação) não está
disponível — o EME não é uma Norma Aberta. E visto que há a intenção de inserir
o EME no HTML (actualizando parte da sua especificação), então o HTML irá ter
uma extensão (EME) que tem “dependências em formatos ou protocolos que não
cumpram, eles próprios, a definição de uma Norma Aberta”. Sim, aprovar o EME
iria fazer com que o HTML deixasse de ser uma Norma Aberta.

## Resumindo

O HTML é uma Norma Aberta, e isso é bom para todos. Tão bom, na realidade, que
alguns países, como Portugal, obrigam a que a Administração Pública use apenas
Normas Abertas. Contudo, existe uma proposta (EME) que, se for aprovada, irá
fazer com que o HTML deixe de ser uma Norma Aberta. Isso irá ter um elevado
impacto negativo em Administrações Públicas, cidadãos, e a comunidade web no
geral — o que hoje em dia significa os cidadãos do mundo.

Esta aprovação iria ter uma elevado impacto social, político e cultural em todo
o lado. Da actual era de partilha de conhecimento, fácil acesso à informação, e
na generalidade tornar o mundo mais acessível, podemos estar a caminhar para a
idade das trevas da web. Há, claro, uma solução: vamos [fazer com que a W3C
saiba que nos opomos ao EME][5], e que pedimos-lhes para que não o aprovem.

[1]: https://fsfe.org/activities/os/def.en.html
[2]: https://m6.ama.pt/docs/Lei362011-NormasAbertas.pdf
[3]: https://w3c.github.io/encrypted-media/#htmlmediaelement-extensions
[4]: https://www.w3.org/2016/03/EME-factsheet.html
[5]: https://drm-pt.info/2016/09/08/contra-drm-na-w3c/
