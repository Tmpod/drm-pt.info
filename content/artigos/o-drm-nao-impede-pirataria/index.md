---
title: O DRM não impede a pirataria
author: Paula Simões
date: 2013-04-20 23:17:00
carrousel: true
categories:
- Factos
tags:
- DRM
- Pirataria
---

O DRM não impede a pirataria simplesmente porque coloca nas mãos do atacante o
cadeado e a chave que o abre.

O DRM baseia-se em códigos, encriptação, e em chaves que desencriptam esses
códigos e a maneira como funciona pode ser explicada de uma forma muito
simples:

Os titulares de direitos (editoras, distribuidoras, produtoras, etc) colocam
cadeados nos conteúdos (filmes, música, livros digitais, jogos de computador,
etc) e escondem as chaves que abrem esses cadeados nos equipamentos e sistemas
onde esses conteúdos vão ser usados (hardware, como o leitor de DVD, e
software, como o programa de computador que vocês utilizam para ver o DVD).

**Ora, nenhum sistema que coloca nas mãos do atacante o cadeado e a chave que
abre esse cadeado pode ser considerado seguro.**

Donde se conclui que, por definição, o DRM nunca poderá impedir a pirataria.
Esta é a razão pela qual sempre que surge um novo DRM, ele é quebrado numa
questão de horas ou dias. Por definição, um sistema de DRM nunca poderá impedir
a pirataria.

> “DRM systems are usually broken in minutes, sometimes days.
> Rarely, months. It’s not because the people who think them up are
> stupid. It’s not because the people who break them are smart.
> It’s not because there’s a flaw in the algorithms. At the end of
> the day, all DRM systems share a common vulnerability: they
> provide their attackers with ciphertext, the cipher and the key.
> At this point, the secret isn’t a secret anymore.“ [1]

[1] – Doctorow, Cory. “Content”. Página 7 do livro, em papel, também disponível
com uma licença Creative Commons em http://craphound.com/content/download/.
Para compreender porque é que o DRM não funciona, aconselhamos a leitura do
primeiro capítulo do livro sugerido, com o título “Microsoft Research DRM Talk”
