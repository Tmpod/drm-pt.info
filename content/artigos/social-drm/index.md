---
title: Social DRM
author: Marcos Marado
date: 2013-04-21 17:59:00
categories:
- DRM Social
---

“DRM Social” é um termo que, tal como “DRM”, é enganador. O argumento é que o
“DRM Social” é um “DRM que não restringe tecnologicamente” e portanto é social.
O contra-argumento é que o “DRM Social” continua a restringir, logo não é
social mas sim anti-social.


## Mais informação

Mais informação em <http://mindboosternoori.blogspot.com/2008/11/social-drm-is-anti-social.html>.
