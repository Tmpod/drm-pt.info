---
title: O DRM impede a utilização educativa
author: Paula Simões
date: 2013-04-25 17:59:00
carrousel: true
categories:
- Factos
---

A lei portuguesa lista várias excepções ao direito de autor, no artigo 75º do
Código de Direito de Autor e Direitos Conexos. Cinco dessas excepções estão
directamente relacionadas com a utilização das obras para fins educativos. Isto
porque professores, alunos e investigadores precisam de utilizar as obras para
o seu trabalho.

No entanto, a lei portuguesa proíbe desde 2004 a neutralização do DRM, mesmo
para fins legais, como é a utilização educativa. Ora, a utilização de obras no
mundo digital pressupõe sempre a cópia, mas a única forma de fazer uma cópia de
uma obra com DRM é neutralizando esse DRM. Donde quando a lei proíbe a
neutralização do DRM para fins legais, está também a proibir a utilização da
obra para fins educativos.

Vejamos um exemplo:

Imaginemos um professor que quer mostrar um excerto de um filme ou de um
documentário à sua turma para introduzir a discussão de uma matéria.

Este professor dirige-se à sua sala de aula com o DVD, que requisitou na
biblioteca, coloca o DVD no computador ligado ao projector e mostra aos alunos
o excerto necessário à discussão. A lei permite isto.

Vamos agora transpor a situação acima descrita para o mundo digital:

Imaginemos que a sala de aula deste professor não tem quatro paredes, mas é um
Sistema de Gestão Educativa, como o Moodle, por exemplo, em que os alunos
acedem, à distância, a um site, com as suas credenciais.

A correspondência com a situação anterior seria o professor copiar o excerto do
filme ou documentário e colocar no fórum do Moodle para os alunos verem e
iniciarem a discussão. Mas a grande maioria dos DVD têm DRM e a lei não permite
a sua neutralização, mesmo para fins legais. Assim, o professor não pode usar o
excerto do filme ou documentário para mostrar aos alunos. A lei menciona mesmo
uma pena de prisão para quem o fizer.
