---
title: Estudo da APDSI sobre DRM
author: Marcos Marado
date: 2007-06-28 21:44:00
coverdescription: |
  Um rolo de etiquetas autocolantes amarelos da campanha DefectiveByDesign.org.
  Inclui o texto "Warning DRM: Product restricts usage or invades privacy.
  DefectiveByDesign.org". À esquerda do está o logótipo da campanha, um
  triângulo de cantos arredondados com uma representação em ASCII de uma cara
  zangada >:(
---

No passado dia 28 de Junho, pelas 18h, na Fundação Portuguesa de Comunicações
(Rua do Instituto Industrial, 16 — junto ao Cais do Sodré em Lisboa), a APDSI
apresentou um estudo sobre o DRM:

- [Site oficial do evento][1]
- [Nota de imprensa][2]
- [Relatório][3]


## O evento

Várias pessoas, representando o DRM-PT, estiveram presentes no evento:

- [Marcos Marado][4] (inscrição confirmada pela APDSI)
- RuiSeabra (inscrição confirmada pela APDSI)
- Pedro Silva (inscrição confirmada pela APDSI)
- Manuel Silva (inscrição confirmada pela APDSI)


## O que foi feito?

- Estivemos presentes e questionámos as posições que ofendiam os direitos dos consumidores
- Distribuimos este panfleto


[1]: http://www.apdsi.pt/main.php?srvacr=pages_43&mode=public&template=frontoffice&layout=layout&id_page=145
[2]: http://www.apdsi.pt/getfile.php?id_file=600
[3]: http://www.apdsi.pt/getfile.php?id_file=599
[4]: http://mindboosternoori.blogspot.com/
