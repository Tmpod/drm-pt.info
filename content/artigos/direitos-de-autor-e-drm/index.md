---
title: Direitos de Autor e DRM
author: Marcos Marado
date: 2015-04-21 20:08:00
coverdescription: Poster do “Workshop de Direitos de Autor”
covercaption: Poster do “Workshop de Direitos de Autor”
---

*Um Workshop sobre Direitos de Autor e Digital Rights Management e um macaco no
cartaz? Está perdido? Nós esclarecemos…mas só um bocadinho: este famoso macaco,
da espécie Macaca Nigra, mete Ellen DeGeneres “no bolso” no que respeita a
tirar selfies. O resto da história fica para 9 de maio!*

Já ouviu falar da Cópia Privada? Sabe o que é DRM? Numa era digital, a
produção, o uso e a reutilização de conteúdos torna-se cada vez mais banal. Mas
qual é a relação entre aquilo que fazemos diariamente nos meios digitais com os
direitos de autor? E será que estes estão adaptados às inovadoras formas de
criar e partilhar?

Num ambiente informal de partilha de experiências e aprendizagem, o ALL e a
ANSOL juntam-se e convidamo-lo a passar connosco uma tarde a falar sobre todos
estes assuntos que estão, cada vez mais, na ordem do dia. A sessão tem início
marcado para as **14H00** do próximo dia **9 de maio**, na **Escola Secundária
Marques de Castilho** e conta com a dinamização de Marcos Marado, Paula Simões
e Teresa Nobre.

A entrada é gratuita mas os lugares são limitados, por isso inscreva-se hoje
mesmo em http://all.cm-agueda.pt/.
