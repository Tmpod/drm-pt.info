---
title: EMI
author: Marcos Marado
date: 2007-07-03 23:44:00
categories:
- Editoras
aliases:
- "/2007/07/03/95/"
---

EMI é a sigla de Electric and Musical Industries Ltd; é uma gravadora
localizada na Inglaterra, mais precisamente em Londres e que mantém operações
em 25 países; a EMI Group é uma das 4 maiores gravadoras do mundo.

Para mais informações gerais sobre a EMI, ver [aqui][1].


## EMI e o DRM

A EMI é adepta do DRM. Em Abril de 2007 a [EMI começou a vender música sem DRM
no iTunes][2], mas continua a vender também a versão com DRM. Além disso, a sua
estratégia quanto ao DRM não é global: não só continua a fazer lançamentos de
CD’s com DRM, especialmente na Europa (Portugal inclusivé), como [vai vender
músicas com DRM em mercados como o Russo][3].

A experiência da venda de música sem DRM no iTunes tem sido dada por eles como
[positiva][4].


[1]: http://pt.wikipedia.org/wiki/EMI
[2]: http://mindboosternoori.blogspot.com/2007/04/apple-itms-to-sell-drm-free-emi-tracks.html
[3]: http://webplanet.ru/english/2007/05/24/sonya_emi_en.html
[4]: http://remixtures.com/2007/06/vendas-de-musica-sem-drm-da-emi-promissoras/
