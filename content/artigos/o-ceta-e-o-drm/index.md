---
title: "O CETA e o DRM"
author: Marcos Marado
date: 2016-10-16 21:07:00
categories:
- Legislação
tags:
- CETA
coverdescription: Protesto contra o CETA em Bratislava. Imagem da Plataforma Não ao TTIP.
covercaption: Protesto contra o CETA em Bratislava. Imagem da Plataforma Não ao TTIP.
---

O CETA é uma proposta de tratado de livre comércio entre a União Europeia e o
Canadá que tem dado ultimamente que falar, não só pela proximidade da data que
se prevê que o tratado seja assinado, mas também pelos inúmeros protestos que
este tem causado por vários sectores da sociedade civil que vêm neste um mau
tratado. bratislava

Mas como está este tratado relacionado com o DRM? Apesar de ser um tratado de
comércio, o CETA é um tratado de enorme abrangência, composto por quase 1600
páginas. Um dos seus objectivos é “proteger as inovações, os artistas e os
produtos tradicionais”.

Dentro desse objectivo, o acordo também se refere ao DRM. Não só o tratado
extende ao Canadá a obrigação de establecer a protecção legal contra a
circumvenção do DRM, algo que já existe na Europa, como, e de forma mais
preocupante, vincula ambas as partes a estagnar nessa condição. Num momento em
que a Europa se prepara para planear a sua reforma em matéria de direitos de
autor, este tratado impede, de facto, que haja qualquer alteração que vá num
sentido inverso ao estipulado no tratado – como a necessária alteração da
legislação vigente no que diz respeito ao DRM.

Assim, os defensores dos direitos dos cidadãos contra o ataque aos seus
direitos e liberdades exercido pela protecção legal dada ao DRM só podem ter
uma coisa a dizer sobre o CETA: ou ele é alterado para deixar de ter cláusulas
que impeçam o desenvolver democrático da Europa, ou ele não deve ser assinado.

---

Saiba mais sobre o CETA [no site da Plataforma contra o TTIP][1].

[1]: http://web.archive.org/web/20160309092812/https://www.nao-ao-ttip.pt/category/ceta/
