---
title: "Projecto de Lei para a resolução do problema do DRM aprovado na generalidade"
author: Marcos Marado
date: 2017-01-08 17:45:00
categories:
- Legislação
coverdescription: Votação que aprova na generalidade o PL sobre DRM
covercaption: Votação que aprova na generalidade o PL sobre DRM
---

A equipa do DRM-PT aplaude as bancadas parlamentares do BE, PAN, PCP, PEV e PS
que, no passado dia 22 de Dezembro, aprovaram na Generalidade o [Projeto de Lei
151/XIII][1], que “Garante o exercício dos direitos dos utilizadores,
consagrados no Código do Direito de Autor e dos Direitos Conexos“, em obras que
contenham DRM. Votação que aprova na generalidade o PL sobre DRM


Este Projecto de Lei, que em nada altera a acção de autores, editores ou
distribuidores, passa a permitir aos cidadãos a circunvenção destas medidas
tecnológicas, quando estas lhes estejam a impedir usos que são considerados
legais.

Apesar de vermos com alguma preocupação os votos contra das bancadas
parlamentares do CDS-PP e do PSD a este projecto, esperamos agora que, de forma
célere, este diploma faça o seu caminho em âmbito de especialidade, e que seja
aprovado e publicado o mais rapidamente possível.

Os cidadãos Portugueses têm-se visto, desde 2004, privados de exercer
legalmente os seus direitos. A reposição desses direitos em 2017, apesar de
tardia, é fundamental, e nossa equipa continuará a seguir e colaborar com o
processo deste Projecto de Lei até à sua publicação.


[1]: http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=40177
