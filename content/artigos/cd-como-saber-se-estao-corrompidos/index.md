---
title: "CD: Como saber se estão corrompidos"
author: Marcos Marado
date: 2011-08-21 21:50:00
categories:
- Música
tags:
- CD
- DRM
- Imagens
- Sony
---

Existem algumas etiquetas que costumam estar presentes num CD corrompido, na
caixa, livro ou mesmo no disco em si. As mais comuns em Portugal são as duas
seguintes:

![IFPI](./ifpi.png)
![Etiqueta de CD com tecnologia BMG](./bmg-cd.jpg)


Um outro método que ajuda a saber se o CD tem ou não protecção contra a cópia é
consultar o [Discogs][1], que tem um campo que nos pode dar esta informação
(ainda que que essa informação dependa do utilizador que inseriu os metadados
daquele CD):

![Captura de ecrã do website Discogs, que mostra o Álbum Cinema de Rodrigo Leão. Na secção "Format", inclui a etiqueta "Protected"](./discogs.png)

[1]: http://www.discogs.com/
