---
title: Acção de Sensibilização sobre DRM
author: Marcos Marado
date: 2007-05-25 18:50:00
coverdescription: Um CD-ROM preso por um aloquete. O aloquete tem nele escrito a palavra DRM.
---

## O quê

Ocorreu no passado dia 25 de Maio de 2007 uma acção de sensibilização sobre
DRM, na semana de estreia do filme “O Pirata das Caraíbas”.


## Porquê?

O Pirata das Caraíbas é um filme sobre uma comunidade diversa que se junta para
combater a tirania opressora da East India Trading Company. No passado dia 14 o
General Gonzales pediu ao Congresso Americano para activar a “Intellectual
Property Protection Act of 2007” (IPPA) que fará com que, entre outras coisas:

- A pena por evitar DRM aumente
- Criminalizar a “tentativa” de infração dos direitos de autor
- Permitir mais escutas para investigar violações aos direitos de autor
- Aumentar as penas para “intenções” de cometer crimes contra direitos de autor

Esta legislação proposta é o resultado do lobie feito por Hollywood, servindo
os seus interesses particulares às custas dos bens púbilcos. Os consumidores
têm-se mostrado contra o DRM, mas grandes corporações transformam a revolta em
crime manipulando governos para a criação de novas leis. A Disney é um dos
principais grupos a favor do DRM em vídeos e filmes, e um preponente para a
extensão permanente dos direitos de autor. A Disney é também um membro fundador
da AACSLA, a organização que licencia o esquema de DRM no HD-DVD e no BluRay.
Eles são a força por detrás da MPAA e um dos maiores instigadores do DMCA nos
USA e do EUCD na Europa – que fazem com que evitar o DRM seja um crime.


## Onde?

Na Lusomundo do Centro Comercial Vasco da Gama, em Lisboa, Portugal.


## Como correu?

Um dos participantes, Rui Seabra, escreveu sobre [como tudo correu][1].

O vídeo do evento pode ser visto aqui:

<https://www.youtube.com/watch?v=-VKZcfohlt8>


[1]: https://web.archive.org/web/20070613162001/http://blog.softwarelivre.sapo.pt/2007/05/26/relatorio-do-protesto-contra-o-drm/
