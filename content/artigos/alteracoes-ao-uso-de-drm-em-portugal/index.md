---
title: Alterações ao uso de DRM em Portugal
author: Marcos Marado
date: 2023-04-30 13:20:00
categories:
- Legislação
tags:
- directiva
- Governo
- transposição
coverdescription: Captura de ecrã que mostra que houve 34 comentários e 33 documentos submetidos na consulta pública do Decreto Lei que transpõe a directiva 2019/790
covercaption: Comentários e contribuições na consulta pública da transposição da directiva 2019/790
---

Nada na mais recente Directiva Europeia sobre o Direito de Autor, aprovada em
2019, implica uma alteração no regime de uso e regulação de DRM como
estabelecido na directiva anterior, de 2001. Ao invés, a directiva reitera o
anteriormente estabelecido, sublinhando no seu artigo 7º que o direito às
utilizações livres não pode ser coartado, nem contractualmente nem por via do
uso de medidas de protecção tecnológica (vulgo DRM).

Acontece que na actual proposta de transposição desta directiva por parte do
Governo Português, não só o legislador opta por mencionar estas medidas, como o
faz não para sublinhar os mecanismos já existentes, mas sim para os restringir,
propondo o acrescento à actual lei da seguinte redacção:

> “sem prejuízo de tais medidas poderem ser utilizadas para limitar o número de
> cópias a efectuar pelo utilizador, a partir de um exemplar legalmente
> adquirido.”

Qual a motivação de acrescentar esta restrição à utilização de obras? E como
pode estar nova restrição compatibilizar-se com a directiva que se tenciona
transpôr?

[A ANSOL respondeu à recente consulta pública sobre esta transposição,
sublinhando este problema][1]. Esperemos agora que o Governo Português tenha em
consideração ete contributo, e que a alteração proposta não figure no texto
final da transposição.

[1]: https://ansol.org/noticias/2023-05-06-consulta-publica/
