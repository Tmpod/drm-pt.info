---
title: Spore
author: Marcos Marado
date: 2008-12-25 00:29:00
categories:
- Jogos
tags:
- EA
- Jogos
- Spore
---

## Um comprador do Spore diz:

Comprei o [Spore][1] no dia em que saiu. Normalmente, testo os jogos antes de
os comprar, mas estava tão excitado com o lançamento do Spore que apenas saí e
comprei-o. Para meu desapontamento, o jogo não só era decepcionante de tão
básico para tentar abranger uma maior audiência, como também continha o
invasivo e draconiano DRM SecuROM. Assim, fico com um jogo medíocre e um mau
sistema a viver no meu computador e que é impossível de remover.

O SecuROM ofendeu tanto a comunidade de partilha de ficheiros que uma versão
sem o DRM do jogo estava disponível mesmo antes do lançamento do jogo. Desde
então, tornou-se um dos jogos mais descarregados de todos os tempos.

O que se torna mais execrável em toda esta história é que enquanto ela destaca
os piores aspectos do DRM, ele poderá aparentar o oposto aos executivos da EA —
este jogo, medíocre, teve um número de downloads ilegais tão grande como
resposta directa aos passos que foram tomados para prevenir a sua cópia.

Duvido que a ironia vá fazer muito para mudar a injusta implementação de DRM
usada pelas inústrias de jogos, música e filmes contra os seus clientes.


## Um defensor do Software Livre comenta

Há muito muito tempo atrás — enquanto esperava pelo lançamento do Duke Nukem
Forever — ouvi rumores de um jogo, um jogo tão maravilhoso e revolucionário no
seu conceito que eu sabia que teria de o jogar. Este jogo não só seria um
simulador de vida, onde poderia criar uma criatura e guiar a sua evolução
durante os vários estados, criando uma maravilhosa e única criatura com os seus
prórios comportamentos e uma aparência completamente única.

Eu sabia que tinha de jogar este jogo. O Spore entrou imediatamente na minha
lista de jogos “a jogar”, tal como o DNF. Com entusiasmo, esperei por todas as
notícias sobre ele que poderia obter.

O tempo foi passando — anos mais tarde eu tornei-me algo como um defensor do
software livre (principalmente graças ao “Windows Genuine Disadvantage”
recusando a activação da minha cópia legítima do Windows) — comecei a usar o
GNU/Linux como Sistema Operativo principal e recorrendo ao Windows apenas para
o jogo ocasional, quando este não corria sobre o Wine (coisa que vem
acontecendo cada vez menos frequentemente).

Descobri uma quantidade enorme de fantásticos jogos de Software Livre:
Tremulous, Alien Arena e o brilhante Scorched3D para falar de alguns. Descobri
que um dos meus jogos favoritos de todos os tempos, Star Control 2, foi lançado
como Software Livre e está agora disponível para quase todas as plataformas com
o título “The Ur-Quan Masters”.

A informação sobre o Spore escasseava ao longo dos tempos, até que no início
deste ano a EA anunciou que o Spore seria lançado com o muito odiado sistema de
DRM conhecido por “SecurROM”, com um conjunto particularmente nefasto de regras
aplicadas:

O Spore só se validaria “falando para casa” a cada dez dias, e apenas
permitiria a sua instalação três vezes. O anúncio causou muito furor, com
muitos a dizer que, com essas condições, não comprariam o jogo.

A EA respondeu cedendo um pouco nas restrições. Continuou a haver muita gente a
refilar, com muitos, incluindo eu, a dizer que não iriam comprar um jogo com
qualquer tipo de DRM, especialmente, o SecuROM, que é conhecido como causador
de problemas nalguns sistemas e é notoriosamente difícil de remover, mesmo após
a desinstalação do jogo.

Decidi que não queria mais o Spore.

Eventualmente, o Spore foi lançado.

Houve mais uma grande revolta contra o DRM que vem com o jogo: foi lançada uma
campanha para votar negativamente o jogo na Amazon, com centenas de reviews
negativas a aparecer e a dizer que o DRM era inaceitável.

Pessoas processaram a EA, porque em lado algum na documentação do jogo há
menções quanto a instalação do SecuROM em conjunto com o jogo.

Apesar de dizer na caixa que o comprador pode ter múltiplas contas na mesma
cópia do Spore, isso não foi suportado inicialmente — se o meu irmão e eu
quisessemos ambos jogar o Spore no mesmo PC e ter cada um o seu perfil, a EA
queria inicialmente que comprássemos duas cópias do jogo!

Depois, as reviews negativas começaram a aparecer: parece que todas as coisas
boas que me faziam realmente curioso em jogar este jogo não chegaram a ser
lançadas na versão final, e o jogo era afinal e essencialmente quatro jogos
medíocres em um, e desenvolver a tua criatura de várias maneiras fazia pouca ou
nenhuma diferença no desenvolvimento do jogo em níveis posteriores.

No meu caso, acabei por não sofrer as consequências do Spore — fui dos sortudos
que soube evitar tudo o que traz DRM, mas sinto pela pelos pobres infelizes que
tiveram malware (SecuROM) instalado no seu sistema pela EA — tudo para jogar um
jogo medíocre que não cumpre o que promete.

Esta semana houve um artigo a apontar o facto que o Spore foi o “Jogo Mais
Descarregado de 2008” — existe agora um vigoroso debate sobre se isso foiou
nãouma resposta directa ao DRM incluído no jogo.

Várias pessoas apontaram para o facto que algumas pessoas descarregaram Spore
para conseguirem jogar o jogo que legitimamente possuem graças aos problemas
com o DRM. Há algo que é abundantemente claro com isso, contudo:

O DRM não funciona: não impede as pessoas de copiar o jogo e, neste caso, está
actualmente a encorajar as pessoas a fazerem-no, visto que as cópias funcionam
melhor que o original!

O Spore foi o mais mediático, mas todos os jogos lançados pela EA depois dele
(Mass Effect, Dead Space, etc.) têm DRM.

Por enquanto pelo menos, parece que a EA sabe que está a fazer um grande erro
mas não está disposta a corrigi-lo.

via [35 Days Against DRM — Day 10: Spore][2]

[1]: https://pt.wikipedia.org/wiki/Spore
[2]: http://www.defectivebydesign.org/day10-spore
