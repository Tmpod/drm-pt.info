---
title: "2016: um ano cheio de DRM"
author: Marcos Marado
date: 2016-12-01 21:49:00
categories:
tags:
- 2016
- BE
- directiva
- DRM
- Governo
- Parlamento
- PCP
- W3C
---

2016 tem sido um ano bastante activo no que diz respeito ao DRM.

Na primeira metade do ano, vimos o Bloco de Esquerda a submeter um [projecto de
Lei][1] que pretende resolver o problema do DRM em Portugal, garantindo as
utilizações Livres ao cidadão. Esta proposta, que contou com os [contributos
escritos da ANSOL e da AEL][2], encontra-se ainda em análise por um Grupo de
Trabalho parlamentar constituído para o efeito, e mantemo-nos atentos ao seu
desenvolvimento.

O grupo de trabalho para a inclusão de DRM na Web não parou o seu trabalho.
Organizámos um [protesto][3] e conversámos com Sir Tim-Berners Lee sobre o assunto,
mas isso não impediu o grupo de trabalho de continuar a tentar atingir o seu
objectivo, e agora a W3C [terá de decidir][4] se quer manter a Web um ambiente
aberto. Também sobre este assunto teremos de nos manter informados.

Ainda sobre o DRM na Web, o Partido Comunista Português [questionou o Governo
Português sobre o que irá fazer sobre o assunto][5], mas, até à data, ainda não
soubemos de uma resposta.

Está também a ser decidido a nível Europeu o CETA, um tratado entre a EU e o
Canadá [que obrigará a Europa a continuar a proteger legalmente o DRM, contra
os cidadãos][6]. A ANSOL foi uma das várias entidades Portuguesas a juntar-se a
centenas de outras, na Europa e no Canadá, a [pedir a rejeição do CETA][7].

E, no meio disto tudo, onde se esperava ver o assunto referido? Na nova
proposta Europeia de Directiva sobre o Direito de Autor, o local certo para
reverter a protecção legal ao DRM garantida pela Directiva de 2001. Em vez
disso, a directiva acaba por [promover ainda mais DRM][8].

Teremos, assim, um 2017 com mais desafios do que nunca.


[1]: https://ensinolivre.pt/?p=625
[2]: https://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=40177
[3]: https://drm-pt.info/2016/09/08/contra-drm-na-w3c/
[4]: https://www.defectivebydesign.org/blog/tim_bernerslee_created_sold_out_web
[5]: http://tek.sapo.pt/noticias/internet/artigo/ansol_e_ael_pedem_ajuda_aos_partidos_na_luta_contra_o_drm-49087jkb.html
[6]: https://drm-pt.info/2016/10/16/o-ceta-e-o-drm/
[7]: https://www.grain.org/bulletin_board/entries/5609-european-and-canadian-civil-society-groups-call-for-rejection-of-ceta
[8]: https://fsfe.org/news/2016/news-20160928-01.en.html
