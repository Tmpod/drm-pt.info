---
title: BE quer resolver problema do DRM com Projecto de Lei
author: Paula Simões
date: 2013-04-27 19:30:00
categories:
- Legislação
---

<figure style="float: right; max-width: 150px;">
  <img src="./authorsdrm.png" alt="Símbolo do movimento Authors Against DRM">
  <figcaption><a href="https://web.archive.org/web/20130415183327/http://readersbillofrights.info/">Readers' Bill of Rights</a></figcaption>
</figure>

**O Bloco de Esquerda entregou na Assembleia da República um [Projecto de Lei][1]
que, se for aprovado, resolve os problemas do DRM.**

**Editado:** Acrescenta-se que o PCP apresentou um projecto de lei sobre a
mesma temática e que ambos vão ser **debatidos no Parlamento no próximo dia 12 de
Junho (quarta-feira) devendo ser votados no dia 14 de Junho (sexta-feira).**

A situação actual, que se mantém há nove anos, permite que os detentores de
direitos proíbam as utilizações livres do Código de Direito de Autor e Direitos
Conexos, a que os cidadãos têm direito.

Alguns exemplos de como o DRM tem sido e continua a ser prejudicial aos
cidadãos:

- Se o leitor comprar um livro digital na LeYa, Wook ou Bertrand, que têm DRM,
  só poderá ler esse livro nos dispositivos que aquelas editoras e
  distribuidoras quiserem. Se o leitor converter o livro que comprou para o
  poder ler no Kindle, a lei estipula uma pena de prisão que pode ir até um
  ano.
- Se o cidadão comprar um DVD – a maioria dos DVD têm DRM -, e quiser fazer uma
  cópia para outro DVD, para o filho não riscar o DVD original ou simplesmente
  para passar o filme para o seu tablet, não pode fazê-lo.
- Se o cidadão comprar um DVD de região 1, por exemplo de importação, mas o seu
  leitor de DVD tiver sido comprado em Portugal com região 2, o cidadão não
  pode ver o DVD.
- Se o professor quiser mostrar um excerto de um filme ou documentário de um
  DVD que requisitou na biblioteca da escola ou da universidade, aos seus
  alunos, para introduzir uma discussão sobre uma determinada matéria e a sala
  de aula deste professor for uma plataforma de ensino à distância, como o
  Moodle, por exemplo, a lei não permite que o professor o faça.
- Se uma biblioteca, um arquivo ou um museu quiser começar a preservar obras
  digitais e estas tiverem DRM, também não o poderão fazer.
- etc.

Mas estas acções só são proibidas porque a lei não permite a neutralização do
DRM pelo consumidor, **mesmo que os fins sejam legais.**

<figure style="float: left; max-width: 150px;">
  <img src="./readersdrm.png" alt="Símbolo do movimento Readers Against DRM">
  <figcaption><a href="https://web.archive.org/web/20130415183327/http://readersbillofrights.info/">Readers' Bill of Rights</a></figcaption>
</figure>

Se o cidadão quiser fazer alguma das acções descritas acima, tem de ir à IGAC
pedir os meios para as fazer. Mas a IGAC não tem, nem nunca teve esses meios e,
portanto, não os dá aos cidadãos. Mesmo que a IGAC tivesse e desse estes meios
aos cidadãos, isso significaria que o consumidor compraria um livro digital e
em vez de, em dois minutos, o converter para o seu dispositivo de leitura teria
de fazer um pedido à IGAC e teria de esperar que a IGAC lhe desse os meios para
fazer essa conversão para poder ler o livro, o que demoraria, no mínimo, dias.
Ora, isto não faz sentido.

O Projecto de Lei do BE vem resolver este problema. O DRM continua a estar
protegido, no caso de acções ilegais, mas não quando os consumidores quiserem
fazer uma acção legal.

**Os consumidores esperam poder usar os conteúdos que compram onde quiserem. Mas
isto só acontecerá se este projecto de lei for aprovado:**

> People are getting used to seamless services: anything, anywhere, on any
> device. They expect that from “old” media too. – Nellie Kroes,
> Vice-Presidente para a Digital Agenda da Comissão Europeia

<figure style="float: right; max-width: 150px;">
<img src="./librariansdrm.png" alt="Símbolo do movimento Librarians Against DRM">
<figcaption><a href="https://web.archive.org/web/20130415183327/http://readersbillofrights.info/">Readers' Bill of Rights</a></figcaption>
</figure>

Um outro ponto crucial neste projecto de lei é a protecção das obras em domínio
público, ou seja, de obras de autores que morreram há mais de 70 anos e que já
não têm direitos patrimoniais. O projecto de lei vem clarificar a não permissão
de colocar DRM nestas obras. A colocação de DRM em obras em domínio público
limita a utilização das mesmas, impedindo a preservação das obras digitais
pelas bibliotecas e arquivos.

**É importante apelar aos srs. Deputados para aprovarem estes Projectos de Lei.
A ANSOL criou uma lista para ser mais fácil contactar os srs. Deputados, [que
podem ver aqui][3].**


[1]: https://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=37676
[2]: https://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=37773
[3]: https://ansol.org/politica/ar/deputados
