---
title: Web Environment Integrity - mais DRM para a Web
author: Marcos Marado
date: 2023-09-02 16:40:00
categories:
- Legislação
tags:
- web
- www
- WEI
- google
- w3c
coverdescription: uma selecção de open issues no repositório do WEI, em que se vê a impopularidade do mesmo
covercaption: uma selecção de open issues no repositório do WEI, em que se vê a impopularidade do mesmo
---

Avisámos dos perigos de aceitar [DRM na Web][1], e infelizmente os nossos medos
confirmaram-se: espalhou-se um pouco por todo o lado (até ao arrepio da lei,
como é o caso da actual [utilização de DRM no site da RTP][2]), trouxe
[violações à privacidade de quem navega na web][3], e a Google [acabou a
dominar o mercado][4], tanto de browsers, com o Chrome, como de DRM providers,
com a Widevine.

Mas todo esse controlo não é "suficiente", quando se pode ter mais, e a Google
não se contenta com o que tem. Para aumentar o controlo que tem - sobre quem
publica sites, o software para os publicar, o software para os visitar e quem
os visita - está a [propor agora o "Web Environment Integrity"][5], uma nova
forma de DRM para a Web.

Ao contrário do que se passou com o EME, no WEI a Google não tentou primeiro
submeter isto à W3C, passando logo para a fase de implementação - [os browers
Chrome e Chromium já começaram a incluir parte desta proposta][6], e [a W3C
comunicou a sua posição neutra até que haja uma submissão formal][7]. A
desculpa que se dá é a do negócio dos anúncios: de como há tanta gente que faz
sites com anúncios à espera de ter aí o seu ganha-pão (e, lembremo-nos, a
Google tem [39% do mercado da publicidade na web][8]), mas depois há quem
instale "ad blockers" para não ver anúncios. O WEI vem "impedir isso" - os
sites para serem vistos têm de ser vistos por navegadores cuja integridade é
validada. Na prática, isto significa que estes sites só vão funcionar com os
browsers aprovados e extensões aprovadas. Como é [dito no Defective By
Design][9],

> Vai ser usado por governos para garantir que apenas os seus navegadores
> oficialmente "aprovados" (leia-se: com backdoors) podem aceder à Internet;
> vai ser usado por corporações como a Netflix para aumentar a Gestão Digital
> de Restrições (DRM); vai ser usado pela Google para negar acesso aos seus
> serviços a não ser que esteja a usar um navegador que aumenta a sua margem de
> lucro.

Tudo isto e muito mais passa a ser possível com a implementação de WEI na Web,
e a troco de nada: quem navega na web só tem a perder com esta nova tecnologia.
Temos todas as razões para dizer 'NÃO' ao WEI, e nenhuma para achar que pode
ser uma boa ideia.

[1]: https://drm-pt.info/2016/09/18/html-eme-e-normas-abertas/
[2]: https://github.com/marado/RNID/issues/99
[3]: https://people.irisa.fr/Gwendal.Patat/assets/pdf/your_drm_can_watch_you_too_slide_pets.pdf
[4]: https://boingboing.net/2019/05/29/hoarding-software-freedom.html
[5]: https://github.com/RupertBenWiser/Web-Environment-Integrity
[6]: https://github.com/chromium/chromium/commit/6f47a22906b2899412e79a2727355efa9cc8f5bd
[7]: https://www.w3.org/blog/2023/web-environment-integrity-has-no-standing-at-w3c/
[8]: https://www.statista.com/statistics/290629/digital-ad-revenue-share-of-major-ad-selling-companies-worldwide/
[9]: https://www.defectivebydesign.org/blog/web_environment_integrity_is_an_all_out_attack_on_free_internet
