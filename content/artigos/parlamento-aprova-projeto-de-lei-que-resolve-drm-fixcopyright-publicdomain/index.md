---
title: "Parlamento aprova projeto de lei que resolve #DRM #FixCopyright #PublicDomain"
author: Paula Simões
date: 2017-04-08 20:49:00
destaque: true
categories:
- Legislação
tags:
- DRM
- Parlamento
coverdescription: No More Locks by Paula Simões Creative Commons Attribution
covercaption: No More Locks by Paula Simões Creative Commons Attribution
---

Foi ontem aprovado o projeto de lei do Bloco de Esquerda, que garante aos
cidadãos poderem realizar as utilizações livres, mesmo que as obras tenham DRM.
Falamos da utilização para fins de ensino e de investigação científica, da
cópia privada, entre outras cujas condições de utilização podem ser verificadas
no [artigo 75º][1] e seguintes do Código de Direito de Autor e Direitos Conexos.

Depois de trabalhado e aprovado na [Comissão de Cultura, Comunicação, Juventude
e Desporto][2], o projeto foi aprovado em plenário com os votos a favor dos grupos
parlamentares do BE, PS, PCP, Verdes, PAN, com a abstenção do grupo parlamentar
do CDSPP e os votos contra do grupo parlamentar do PSD.

Para além de permitir a realização das utilizações livres, o projeto de lei
interdita ainda a colocação de DRM em obras caídas no domínio público (que já
não têm direitos de autor patrimoniais), protegendo o nosso património
cultural, bem como interdita ainda a colocação de DRM em obras editadas por
entidades públicas ou com financiamento público.

A Associação Ensino Livre e a [ANSOL][3], dinamizadores do movimento [DRMPT][4]
congratulam o Bloco de Esquerda, o Partido Socialista, o Partido Comunista
Português, o Partido Ecologista Os Verdes e o Partido Pessoas, Animais e
Natureza pela inciativa, trabalho e apoio em garantir que os cidadãos possam
finalmente exercer os seus direitos fundamentais também no que respeita a obras
com DRM.

O projeto de lei aprovado terá ainda de ser promulgado pelo Sr.Presidente da
República.

[O texto final pode ser consultado neste link [PDF]][5].

[1]: http://www.pgdlisboa.pt/leis/lei_mostra_articulado.php?artigo_id=484A0075&nid=484&tabela=leis&pagina=1&ficha=1&so_miolo=&nversao=#artigo
[2]: http://www.parlamento.pt/sites/com/XIIILeg/12CCCJD/Paginas/default.aspx
[3]: https://ansol.org/
[4]: https://drm-pt.info/
[5]: http://app.parlamento.pt/webutils/docs/doc.pdf?path=6148523063446f764c324679626d56304c334e706447567a4c31684a53556c4d5a5763765130394e4c7a457951304e44536b51765247396a6457316c626e527663306c7561574e7059585270646d46446232317063334e68627938774f474a694e6a41784f53307959325a684c545135596a5174596a67355969316a4e7a466c596a466d4d6a49354d5749756347526d&fich=08bb6019-2cfa-49b4-b89b-c71eb1f2291b.pdf&Inline=true
