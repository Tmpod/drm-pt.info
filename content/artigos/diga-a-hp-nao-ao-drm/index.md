---
title: "Diga à HP: Não ao DRM!"
author: Marcos Marado
date: 2016-10-02 18:18:00
---

A HP activou recentemente uma funcionalidade secreta nas suas impressoras
Officejet Pro (e possivelmente outros modelos) que faz com que as impressoras
recusem imprimir com tinteiros de outras companhias ou tinta reciclada.

Juntos, podemos enviar uma forte mensagem à HP: Digam não ao DRM.

A HP é apenas a mais recente das empresas a usar tecnologia DRM para colocar
limitações artificiais ao que você pode fazer com a sua propriedade. A
legislação de direitos de autor da Europa e dos Estados Unidos incentiva as
empresas tecnológicas a usar DRM: sob a alçada do Digital Millenium Copyright
Act (nos Estados Unidos) ou a Directiva para a Sociedade de Informação (na
Europa), os fabricantes dizem que é ilegal dar a volta ao DRM nas coisas que
você possui, mesmo que o esteja a fazer para fins legais.

O DRM é mau para a inovação, é mau para os direitos dos utilizadores, e é um
desastre de segurança. Por favor junte-se a nós a exigir à HP que corrija as
suas impressoras autodestrutivas.

[Diga à HP: Não ao DRM!][1]

PS: A EFF está também a considerar apresentar uma queixa à FTC sobre o uso de
DRM nas impressoras da HP. Se a recente actualização de software o afectou, por
favor entre em contacto com a EFF e conte a sua história. Se tiver um amigo que
foi afectado pelo DRM nas impressoras HP, por favor faça-lhe chegar este texto.

---

Esta é uma [campanha da EFF][2]. O texto e a imagem aqui utilizados estão
licenciados com uma licença [CC-BY 3.0 US][3].

[1]: https://act.eff.org/action/tell-hp-say-no-to-drm
[2]: https://act.eff.org/action/tell-hp-say-no-to-drm
[3]: https://www.eff.org/copyright
