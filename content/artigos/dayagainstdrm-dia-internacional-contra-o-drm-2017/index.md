---
title: "#DayagainstDRM – Dia Internacional contra o DRM 2017"
author: Marcos Marado
date: 2017-07-09 19:14:00
coverdescription: Fotografia por Paula Simões, Creative Commons BY
covercaption: Fotografia por Paula Simões, Creative Commons BY
---

DRM, Gestão de Restrições Digitais (Digital Restrictions Management) são
mecanismos que os fabricantes põem nos produtos e serviços para restringir
certas utilizações pelos consumidores. Não conseguir fazer uma cópia de um DVD;
não conseguir ler um livro digital em qualquer leitor; não conseguir extrair um
excerto de um livro digital ou filme para usar nas aulas; não conseguir
imprimir usando tinteiros de outras marcas; ser obrigado a estar online quando
se quer jogar um jogo, tudo isto são exemplos de como o DRM nos afecta no dia a
dia.

Estes mecanismos impedem frequentemente utilizações que seriam legais, e muito
comuns, de obras, ferramentas e dispositivos. Na prática, os cidadãos
encontram-se assim privados de muitos dos seus direitos, a não ser que
contornem o DRM.

Nos últimos 13 anos, o acto de contornar o DRM era crime em Portugal, punível
com até dois anos de prisão. No dia de 4 de Junho deste ano [entrou finalmente
em vigor uma alteração à lei que permite aos consumidores neutralizar o DRM
para fins legais][1]. Foi um passo importante para devolver aos cidadãos os
seus direitos, que está a ser observado com interesse por outros países.

Apesar de, em Portugal, agora ser possível contornar o DRM nestas situações, o
seu impacto é limitado pelo facto de na maioria de outros países continuar a
ser proibido, incluindo todos os outros Estados-Membros da União Europeia. Além
disso, está planeada uma expansão da utilização e da protecção legal ao DRM.

No final do ano passado, a Comissão Europeia propôs uma alteração à Directiva
da Sociedade da Informação que está a ser discutida pelo Parlamento Europeu.
Vários deputados europeus submeteram emendas que permitiriam aos cidadãos
neutralizar o DRM para fins legais, à semelhança do que foi feito em Portugal.

ANSOL, AEL, D3 e FSF, entre outras organizações, juntam-se hoje a nós e
milhares de cidadãos, para espalhar a mensagem: “Não ao DRM”. Para além de
marcar este dia, há muito a fazer para garantir os direitos dos cidadãos. Por
exemplo, é [importante contactarmos os nossos representantes no Parlamento
Europeu][2] para que eles votem a favor das emendas que melhoram a situação
legal.

Queres começar já a ajudar o movimento contra o DRM? Partilha este artigo!

- ANSOL – Associação Nacional para o Software Livre https://ansol.org/
- AEL – Associação Ensino Livre https://ensinolivre.pt
- Associação D3 – Defesa dos Direitos Digitais https://direitosdigitais.pt/
- Defective By Design –  https://dayagainstdrm.org


[1]: https://ensinolivre.pt/?p=797
[2]: https://www.europarl.europa.eu/meps/en/search.html?country=PT
