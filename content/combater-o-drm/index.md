---
title: Combater o DRM
---

- ~~Inscrevendo-se na nossa [Mailing List][1]~~
- Não comprando produtos com DRM ([carrega aqui para saber como reconhecê-los][5])
- Boicotando empresas que vendam produtos com DRM ([carrega aqui para saber como reconhecê-las][5])
- Ajudando a melhorar este site
- Tornando-se [sócio da ANSOL][2] e/ou da [AEL][3]
- Existem também campanhas internacionais, como a [Defective By Design][4], contribua!

[1]: http://listas.drm-pt.info/cgi-bin/mailman/listinfo/drm-nao
[2]: https://ansol.org/inscricao
[3]: https://ensinolivre.pt/?q=node/118
[4]: https://www.defectivebydesign.org/
[5]: https://drm-pt.info/como-descobrir-drm/
