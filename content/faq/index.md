---
title: Perguntas Frequentes
---

Segue-se uma lista de perguntas frequentes e respectivas respostas. Se tem uma
dúvida em relação a este assunto, e quiser vê-la respondida, pode adicionar a
sua questão nos comentários e ela será respondida.


## Quais são os meus direitos actuais?

Se uma obra com DRM o impedir de fazer algo a que teria legalmente direito
(cópia privada, por exemplo, ou qualquer outra utilização livre do Código de
Direito de Autor e Direitos Conexos), pode pedir o conteúdo, sem restrições, à
IGAC. Infelizmente eles ir-lhe-ão responder que não têm esse conteúdo, e
recusar-se-ão a fazer algo para o ter…


## Porque é que o DRM é mau para a liberdade dos utilizadores de Software?

O DRM é incompatível com Software Livre. DRM é apenas possível tornando algumas
partes dum programa de computador um segredo para os utilizadores e
não-modificável, o que é um ataque directo às liberdades dos utilizadores. O
DRM não consegue funcionar enquanto sendo Software Livre visto que isso iria
permitir que as medidas de restrição pudessem ser eliminadas.


## Devo incentivar aqueles que me tratam como um ladrão?

Não. A equipa do DRM-PT incentiva o boicote a produtos com DRM e aqueles que os promovem.


## Existem formatos/dispositivos que tentam limitar os meus direitos?

Sim, os formatos ou dispositivos que tenham tecnologias de DRM estão a a limitar os seus direitos.


## É legal essa tentativa de limitação dos meus direitos? Que posso fazer?

Ver “Quais são os meus direitos actuais?”


## O que é DRM?

A resposta encontra-se na página ["O que é o DRM"][1].


## Mas o DRM Social já não é mau, pois não?

A resposta encontra-se na página ["Social DRM"][2].


## Como saber da existência de DRM num produto?

A resposta encontra-se na página ["Como descobrir DRM"][3].


## Como combater o DRM?

A resposta encontra-se na página ["Combater o DRM"][4].


[1]: https://drm-pt.info/o-que-e-drm/
[2]: https://drm-pt.info/2013/04/21/social-drm/
[3]: https://drm-pt.info/como-descobrir-drm/
[4]: https://drm-pt.info/combater-o-drm/
