# Código fonte do website drm-pt.info

Plataforma dedicada à divulgação de informação sobre DRM em Portugal.


## Tarefas em falta

- [x] Pesquisa
- [ ] Garantir acessibilidade, foco no carrossel na página principal
- [x] Garantir que o link antigo de RSS funciona (`/feed/`)
- [ ] Meter o carrossel a andar sozinho com um bocadinho de javascript
- [ ] Remover google fonts (Oswald)
- [ ] Fazer upload dos vídeos do youtube para o viste.pt e adicionar embeds
- [ ] Substituir `/img/logo.png` por versão de melhor qualidade
- [ ] Fazer self-host do stork.{js,wasm}
